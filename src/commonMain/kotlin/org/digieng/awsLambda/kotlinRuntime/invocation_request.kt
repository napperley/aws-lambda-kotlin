package org.digieng.awsLambda.kotlinRuntime

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class InvocationRequest(
    /** The payload is represented as a *UTF-8* string. */
    val payload: String = "",
    /** An identifier unique to the current invocation. */
    @SerialName("request_id")
    val reqId: String,
    @SerialName("xray_trace_id")
    val xrayTraceId: String = "",
    @SerialName("function_arn")
    val functionArn: String
)

/** The number of milliseconds left before lambda terminates the current execution. */
expect fun InvocationRequest.timeRemaining(): Long
