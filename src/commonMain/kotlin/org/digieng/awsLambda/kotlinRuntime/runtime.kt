@file:Suppress("EXPERIMENTAL_API_USAGE")

package org.digieng.awsLambda.kotlinRuntime

internal const val LAMBDA_REQUEST_ID_HEADER = "lambda-runtime-aws-request-id"
const val RUNTIME_VER: String = "0.1.0"
val runtimeMajorVer: UInt
    get() = RUNTIME_VER.split('.')[0].toUInt()
val runtimeMinorVer: UInt
    get() = RUNTIME_VER.split('.')[1].toUInt()
val runtimePatchVer: UInt
    get() = RUNTIME_VER.split('.')[2].toUInt()

/** First element is the invocation request, second element is the response code. */
internal typealias NextOutcome = Pair<InvocationRequest, Int>

/** The Kotlin runtime for AWS Lambda where all the *key* stuff occurs. */
internal expect class Runtime(endpoint: String) : Closable {
    /** Ask AWS Lambda for an invocation. */
    suspend fun fetchNext(): NextOutcome

    /** Tells AWS Lambda that the function has succeeded. */
    suspend fun postSuccess(invocationId: String, handlerResp: InvocationResponse.Success)

    /** Tells AWS Lambda that the function has failed. */
    suspend fun postFailure(invocationId: String, handlerResp: InvocationResponse.Failure)

    /** Tells AWS Lambda that there is a runtime initialization error. */
    suspend fun postRuntimeError(handlerResp: InvocationResponse.RuntimeInitFailure)
}

internal fun successHttpCode(httpCode: Int): Boolean {
    val firstSuccessErrorCode = 200
    val lastSuccessErrorCode = 299
    return httpCode in (firstSuccessErrorCode..lastSuccessErrorCode)
}

/**
 * Runs the Kotlin lambda on AWS Lambda.
 * @param handler The Kotlin lambda to use.
 */
expect suspend fun runHandler(handler: (InvocationRequest) -> InvocationResponse)
