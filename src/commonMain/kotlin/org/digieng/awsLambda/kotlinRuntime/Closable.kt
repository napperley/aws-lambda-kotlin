package org.digieng.awsLambda.kotlinRuntime

internal interface Closable {
    fun close()
}
