package org.digieng.awsLambda.kotlinRuntime

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

sealed class InvocationResponse {
    /** Represents a success response. */
    @Serializable
    data class Success(
        @SerialName("content_type")
        val contentType: String = "text/plain",
        /** The payload is represented as a *UTF-8* string. */
        val payload: String = ""
    ) : InvocationResponse()

    /** Represents a failure response. */
    @Serializable
    data class Failure(val errorMsg: String, val errorType: String) : InvocationResponse()

    /** Represents a runtime error response. */
    @Serializable
    data class RuntimeInitFailure(val errorMsg: String, val errorType: String) : InvocationResponse()
}
