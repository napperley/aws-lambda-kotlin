package org.digieng.awsLambda.kotlinRuntime

import io.ktor.client.HttpClient
import io.ktor.client.engine.curl.Curl
import io.ktor.client.request.request
import io.ktor.client.statement.HttpResponse
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.utils.io.readUTF8Line
import kotlinx.cinterop.toKString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import platform.posix.fprintf
import platform.posix.getenv
import platform.posix.stderr

internal actual class Runtime actual constructor(private val endpoint: String) : Closable {
    private val client = HttpClient(Curl)
    private val json = Json(JsonConfiguration.Stable)

    private suspend fun readPayload(invocation: HttpResponse): String {
        val tmp = StringBuilder()
        while (invocation.content.availableForRead > 0) {
            tmp.append("${invocation.content.readUTF8Line()}\n")
        }
        return tmp.toString()
    }

    private suspend fun createInvocationRequest(invocationId: String, invocation: HttpResponse) = try {
        json.parse(InvocationRequest.serializer(), readPayload(invocation))
    } catch (ex: Exception) {
        fprintf(stderr, "Cannot create invocation request for Invocation ID $invocationId: ${ex.message}")
        val status = HttpStatusCode.BadRequest
        postFailure(invocationId, InvocationResponse.Failure(status.description, "${status.value}"))
    }

    actual suspend fun fetchNext(): NextOutcome {
        var respCodeResult = 200
        lateinit var invocationReqResult: InvocationRequest
        // Get next request to process in a long-polling style.
        val invocation = client.request<HttpResponse>("$endpoint/2018-06-01/runtime/invocation/next") {
            method = HttpMethod.Get
        }
        val invocationId = invocation.call.response.headers[LAMBDA_REQUEST_ID_HEADER] ?: ""
        val httpCode = invocation.call.response.status.value
        val invocationReq = createInvocationRequest(invocationId, invocation)
        if (invocationReq is InvocationRequest && successHttpCode(httpCode)) {
            respCodeResult = httpCode
            invocationReqResult = invocationReq
        } else if (invocationReq is InvocationRequest && !successHttpCode(httpCode)) {
            respCodeResult = httpCode
            invocationReqResult = invocationReq
        }
        return (invocationReqResult to respCodeResult)
    }

    actual suspend fun postSuccess(invocationId: String, handlerResp: InvocationResponse.Success) {
        client.request<HttpResponse>("$endpoint/2018-06-01/runtime/invocation/$invocationId/response") {
            method = HttpMethod.Post
            body = TextContent(
                json.stringify(InvocationResponse.Success.serializer(), handlerResp),
                ContentType.Application.Json
            )
        }
    }

    actual suspend fun postFailure(invocationId: String, handlerResp: InvocationResponse.Failure) {
        client.request<HttpResponse>("$endpoint/2018-06-01/runtime/invocation/$invocationId/error") {
            method = HttpMethod.Post
            body = TextContent(
                json.stringify(InvocationResponse.Failure.serializer(), handlerResp),
                ContentType.Application.Json
            )
        }
    }

    actual suspend fun postRuntimeError(handlerResp: InvocationResponse.RuntimeInitFailure) {
        client.request<HttpResponse>("$endpoint/2018-06-01/runtime/init/error") {
            method = HttpMethod.Post
            body = TextContent(
                json.stringify(InvocationResponse.RuntimeInitFailure.serializer(), handlerResp),
                ContentType.Application.Json
            )
        }
    }

    override fun close() {
        client.close()
    }
}

private suspend fun validateLambdaApi(lambdaApi: String, runtime: Runtime): Boolean =
    if (lambdaApi.isEmpty()) {
        fprintf(stderr, "AWS Lambda server address not specified!")
        val resp = InvocationResponse.RuntimeInitFailure("Environment variable AWS_LAMBDA_API isn't set.",
            "${HttpStatusCode.BadRequest.value}")
        runtime.postRuntimeError(resp)
        runtime.close()
        false
    } else {
        true
    }

actual suspend fun runHandler(handler: (InvocationRequest) -> InvocationResponse) {
    println("Initializing the Kotlin Lambda Runtime (v$RUNTIME_VER)...")
    var retries = 0
    val maxRetries = 3
    // Get the address for the function.
    val lambdaApi = getenv("AWS_LAMBDA_API")?.toKString() ?: ""
    val runtime = Runtime("http://$lambdaApi")
    if (validateLambdaApi(lambdaApi, runtime)) {
        while (retries < maxRetries) {
            val (req, respCode) = runtime.fetchNext()
            if (!successHttpCode(respCode)) {
                println("HTTP request was not successful. HTTP response code: $respCode. Retrying...")
                retries++
                continue
            } else {
                retries = 0
                println("Invoking user handler...")
                val resp = handler(req)
                println("Invoking user handler completed.")
                if (resp is InvocationResponse.Success) runtime.postSuccess(req.reqId, resp)
                else if(resp is InvocationResponse.Failure) runtime.postFailure(req.reqId, resp)
            }
        }
        if (retries == maxRetries) fprintf(stderr, "Exhausted all retries.")
    }
}
