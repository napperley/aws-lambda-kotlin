group = "org.digieng"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.3.72"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.3.72"
}

repositories {
    mavenCentral()
    jcenter()
}

kotlin {
    val kotlinxSerializationVer = "0.20.0"
    linuxX64 {
        compilations.getByName("main") {
            dependencies {
                val ktorClientVer = "1.3.2"
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-native:" +
                    kotlinxSerializationVer)
                implementation("io.ktor:ktor-client-curl-linuxx64:$ktorClientVer")
                implementation("io.ktor:ktor-client-json-linuxx64:$ktorClientVer")
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.3.72"
                implementation(kotlin("stdlib-common", kotlinVer))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:" +
                    kotlinxSerializationVer)
            }
        }
    }
}

tasks.create("createLinuxX64CMappings") {
    doFirst { println("Creating C mappings for linuxX64 module...") }
    dependsOn("cinteropLibcurlLinuxX64")
}
